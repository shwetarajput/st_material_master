package com.acceval.integration.serviceImpl;

import com.acceval.integration.dto.Acknowledgement;
import com.acceval.integration.material.entity.MaterialMasterMbewEntity;
import com.acceval.integration.material.entity.MaterialMasterMeraEntity;
import com.acceval.integration.material.entity.MaterialMasterMvkeEntity;
import com.acceval.integration.material.repo.MaterialMasterMbewRepo;
import com.acceval.integration.material.repo.MaterialMasterMeraRepo;
import com.acceval.integration.material.repo.MaterialMasterMvkeRepo;
import com.acceval.integration.service.IoclDataServices;
import com.acceval.integration.service.IoclMaterialDataServices;
import com.acceval.integration.util.IoclDateUtil;
import com.acceval.integration.ws.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InsertMaterialMasterService implements IoclMaterialDataServices {


    private static final Logger logger = LogManager.getLogger(InsertMaterialMasterService.class);

    private MasterDataWebServicePortType servicePort;

    // Initialize the service endpoint
    public InsertMaterialMasterService() {
        MasterDataWebService service = new MasterDataWebService();
        servicePort = service.getMasterDataWebServicePort();
    }

    @Autowired
    private MaterialMasterMbewRepo materialMasterMbewRepo;

    @Autowired
    private MaterialMasterMeraRepo materialMasterMeraRepo;

    @Autowired
    private MaterialMasterMvkeRepo materialMasterMvkeRepo;
    @Autowired
    IoclDateUtil ioclUtil;

    @Override
    public void sendMaterialMasterToSt(String startDate, String endDate) {
        MasterDataMaterialInMessage masterDataReq = null;
        MasterDataStatusMessage response = null;
        Acknowledgement ack = new Acknowledgement();

        ack.setStatus(false);
        ack.setMsg("Unable to send material Master data to ST");

        List<MasterDataStatusMessage> responseList = new ArrayList<MasterDataStatusMessage>();
        try {
            List<MaterialMasterMbewEntity> materialMasterMbewEntities = getMaterialMasterMbew(startDate, endDate);
            List<MaterialMasterMeraEntity> materialMasterMeraEntities = getMaterialMasterMera(startDate, endDate);
            List<MaterialMasterMvkeEntity> materialMasterMvkeEntities = getMaterialMasterMvke(startDate, endDate);
            List<MasterDataMaterialInMessage> masterDataMaterialInMessages = new ArrayList<>();

            if(materialMasterMbewEntities != null){
                for (MaterialMasterMeraEntity materialMasterMeraEntity : materialMasterMeraEntities){
                    MasterDataMaterialInMessage masterDataMaterialInMessage = new MasterDataMaterialInMessage();
                    masterDataMaterialInMessage.setBaseUom(materialMasterMeraEntity.getMeins());
                    masterDataMaterialInMessage.setCode(materialMasterMeraEntity.getMatnr());
                    masterDataMaterialInMessage.setName(materialMasterMeraEntity.getMaktx());
                    List<Item> items = getItemsBasedOnMantr(materialMasterMvkeEntities,materialMasterMeraEntity.getMatnr());
                    masterDataMaterialInMessage.getItems().addAll(items);
                    List<PlantItem> plantItems = getPlantItemsOnMantr(materialMasterMbewEntities,materialMasterMeraEntity.getMatnr());
                    masterDataMaterialInMessage.getPlantItems().addAll(plantItems);

                }
            }
        } catch (Exception e) {
            ack.setStatus(true);
            ack.setMsg("Unable to send customer Master data to ST. Some exception occured :: " + e.getMessage());
            logger.error("Unable to send customer Master data to ST. Some exception occured", e);
        }
    }

    private List<PlantItem> getPlantItemsOnMantr(List<MaterialMasterMbewEntity> materialMasterMbewEntities, String matnr) {

        List<MaterialMasterMbewEntity> mbewEntities= materialMasterMbewEntities.stream().filter(m->m.getMatnr().equalsIgnoreCase(matnr)).collect(Collectors.toList());
        List<PlantItem> plantItemList=null;
        if(materialMasterMbewEntities != null){
            plantItemList= new ArrayList<>();
            for(MaterialMasterMbewEntity materialMasterMbew: mbewEntities){
                PlantItem plantItem = new PlantItem();
                plantItem.setPlant(materialMasterMbew.getBwkey());
                plantItem.setValuationClass(new JAXBElement<>(new QName("valuationClass"), String.class,materialMasterMbew.getBklas()));
                plantItemList.add(plantItem);
            }

        }
        return plantItemList;
    }

    private List<Item> getItemsBasedOnMantr(List<MaterialMasterMvkeEntity> materialMasterMvkeEntities, String matnr) {

        List<MaterialMasterMvkeEntity> mvkeEnities=  materialMasterMvkeEntities.stream().filter(m->m.getMatnr().equalsIgnoreCase(matnr)).collect(Collectors.toList());
        List<Item> items = null;
        if (materialMasterMvkeEntities != null) {
            items= new ArrayList<>();
            for (MaterialMasterMvkeEntity materialMasterMvkeEntity : mvkeEnities) {
                Item item = new Item();
                item.setDeliveryPlant(new JAXBElement<>(new QName("deliveryPlant"), String.class, materialMasterMvkeEntity.getDwerk()));
                item.setDistributionChannel( materialMasterMvkeEntity.getVtweg());
                item.setMaterialGroup1( materialMasterMvkeEntity.getMvgr1());
                item.setMaterialGroup2( materialMasterMvkeEntity.getMvgr2());
                item.setMaterialGroup2( materialMasterMvkeEntity.getMvgr3());
                item.setSalesOrganisation(materialMasterMvkeEntity.getVkorg());
                items.add(item);
            }
        }
        return items;
    }

    private List<MaterialMasterMvkeEntity> getMaterialMasterMvke(String startDate, String endDate) {
        System.out.println("start Date :: " + startDate + " :: end Date :: " + endDate);
        List<MaterialMasterMvkeEntity> materialMasterMvkeEntities = null;
        try {
            if (startDate != null && endDate != null) {

                Date begDate = ioclUtil.convertedDate(startDate, "yyyy-MM-dd HH:mm:ss");
                Date endingDate = ioclUtil.convertedDate(endDate, "yyyy-MM-dd HH:mm:ss");

                materialMasterMvkeEntities = materialMasterMvkeRepo.findAllByCreateddateBetween(begDate, endingDate);

            }
        } catch (Exception e) {
            logger.error("Unable to fetch Material Master data from HOIS. Some exception occured", e);

        }
        return materialMasterMvkeEntities;
    }

    private List<MaterialMasterMeraEntity> getMaterialMasterMera(String startDate, String endDate) {

        System.out.println("start Date :: " + startDate + " :: end Date :: " + endDate);
        List<MaterialMasterMeraEntity> materialMasterMeraEntities = null;
        try {
            if (startDate != null && endDate != null) {

                Date begDate = ioclUtil.convertedDate(startDate, "yyyy-MM-dd HH:mm:ss");
                Date endingDate = ioclUtil.convertedDate(endDate, "yyyy-MM-dd HH:mm:ss");

                materialMasterMeraEntities = materialMasterMeraRepo.findAllByCreateddateBetween(begDate, endingDate);

            }
        } catch (Exception e) {
            logger.error("Unable to fetch Material Master data from HOIS. Some exception occured", e);

        }
        return materialMasterMeraEntities;
    }


    private List<MaterialMasterMbewEntity> getMaterialMasterMbew(String startDate, String endDate) {

        System.out.println("start Date :: " + startDate + " :: end Date :: " + endDate);
        List<MaterialMasterMbewEntity> materialMasterMbewEntities = null;
        try {
            if (startDate != null && endDate != null) {

                Date begDate = ioclUtil.convertedDate(startDate, "yyyy-MM-dd HH:mm:ss");
                Date endingDate = ioclUtil.convertedDate(endDate, "yyyy-MM-dd HH:mm:ss");

                materialMasterMbewEntities = materialMasterMbewRepo.findAllByCreateddateBetween(begDate, endingDate);

            }
        } catch (Exception e) {
            logger.error("Unable to fetch Material Master data from HOIS. Some exception occured", e);

        }
        return materialMasterMbewEntities;
    }
}
