package com.acceval.integration.material.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "yv_material_master_all_views_e_mera", schema = "iocl")
public class MaterialMasterMeraEntity {

    @Id
    private int id;

    private String bism;

    private String brgew;

    private String ersda;

    private String ferth;

    private String groes;

    private String laeda;

    private String maktx;

    private String matnr;

    private String meins;

    private String normt;

    private String ntgew;

    private String tragr;

    private String wrkst;

    private LocalDate createddate;

    private Boolean isActive;

    private LocalDate modifieddate;

}
