package com.acceval.integration.material.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "yv_material_master_all_views_e_mbew", schema = "iocl")
public class MaterialMasterMbewEntity {

    @Id
    private int id;

    private String bklas;

    private String bwkey;

    private String matnr;

    private String stprs;

    private String verpr;

    private String vprsv;

    private LocalDate createddate;

    private Boolean isActive;

    private LocalDate modifieddate;
}
