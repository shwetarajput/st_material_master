package com.acceval.integration.material.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "yv_material_master_all_views_e_mvke", schema = "iocl")
public class MaterialMasterMvkeEntity {

    @Id
    private int id;

    private String dwerk;

    private String kondm;

    private String kondmt;

    private String ktgrmt;

    private String ktgrm;

    private String matnr;

    private String mvgr1;

    private String mvgr2;

    private String mvgr3;

    private String vkorg;

    private String vtweg;

    private LocalDate createddate;

    private Boolean isActive;

    private LocalDate modifieddate;
}
