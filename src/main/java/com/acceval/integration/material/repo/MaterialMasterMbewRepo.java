package com.acceval.integration.material.repo;


import com.acceval.integration.material.entity.MaterialMasterMbewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MaterialMasterMbewRepo extends JpaRepository<MaterialMasterMbewEntity, Long> {

    public List<MaterialMasterMbewEntity> findAllByCreateddateBetween(Date startdate, Date enddate );

}
