package com.acceval.integration.material.repo;


import com.acceval.integration.material.entity.MaterialMasterMeraEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MaterialMasterMeraRepo extends JpaRepository<MaterialMasterMeraEntity, Long> {

    public List<MaterialMasterMeraEntity> findAllByCreateddateBetween(Date startdate, Date enddate );
}
