package com.acceval.integration.material.repo;


import com.acceval.integration.material.entity.MaterialMasterMvkeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MaterialMasterMvkeRepo extends JpaRepository<MaterialMasterMvkeEntity, Long> {

    public List<MaterialMasterMvkeEntity> findAllByCreateddateBetween(Date startdate, Date enddate );
}
